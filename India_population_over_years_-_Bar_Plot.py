# import csv

# import matplotlib.pyplot as plt

# year_list = {}

# with open('population-estimates_csv.csv',mode='r') as file:
#     data = csv.DictReader(file)

#     for row in data:
#         if row['Region'] == "India":
#             if year_list.get(row['Year']) == None:
#                 year_list[row['Year']] = row['Population']
#             else:
#                 year_list[row['Year']] += row['Population']
#             print(row['Year'])

# plt.bar(list(year_list.keys()),list(year_list.values()))

# plt.title("India population over year")

# plt.xticks(rotation = 90)
# plt.tight_layout()
# plt.xlabel("Year")
# plt.ylabel("Population")

# plt.show()


import csv
import matplotlib.pyplot as plt
from csv import DictReader


def Calculate(Pop_Data):
    Year_list = {}

    for row in Pop_Data:
        if row['Region'] == 'India':
            if row['Year'] not in Year_list:
                Year_list[row['Year']] = float(row['Population'])
            else:
                Year_list[row['Year']] += float(row['Population'])
    return Year_list


def Transfer(India_data):
    Year_Name = list(India_data.keys())
    Pop_val = list(India_data.values())
    return Year_Name, Pop_val


def Graph(Year_Name, Pop_Val):
    plt.bar(Year_Name, Pop_Val)
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.show()


def main():
    Csv_data = open('population-estimates_csv.csv', mode='r')
    Pop_data = DictReader(Csv_data)
    India_data = Calculate(Pop_data)
    Year_Name, Pop_val = Transfer(India_data)
    Graph(Year_Name, Pop_val)


if __name__ == "__main__":
    main()
