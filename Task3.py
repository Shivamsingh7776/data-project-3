# import csv

# import matplotlib.pyplot as plt

# year_list = {}
# year = []
# with open('population-estimates_csv.csv', mode='r') as file:
#     data = csv.DictReader(file)

#     for row in data:
#         if row['Region'] == 'Afghanistan' or row['Region'] == 'Bangladesh' or row['Region'] == 'Bhutan' or row['Region'] == 'India' or row['Region'] == 'Maldives' or row['Region'] == 'Nepal' or row['Region'] == 'Pakistan' or row['Region'] == 'Sri Lanka':
#             year_data = row['Year']
#             Pop_data = row['Population']

#             if year_data not in year_list:
#                 year_list[row['Year']] = float(row['Population'])
#             else:
#                 year_list[row['Year']] += float(row['Population'])

# plt.xlabel('Year')
# plt.ylabel('Total SAARC population')
# plt.bar(list(year_list.keys()), list(year_list.values()))
# plt.xticks(rotation=90)
# plt.tight_layout()
# plt.show()

import csv

import matplotlib.pyplot as plt
from csv import DictReader


def Calculate(Pop_Data):
    Year_list = {}
    for row in Pop_Data:
        if row['Region'] == 'Afghanistan' or row['Region'] == 'Bangladesh' or row['Region'] == 'Bhutan' or row['Region'] == 'India' or row['Region'] == 'Maldives' or row['Region'] == 'Nepal' or row['Region'] == 'Pakistan' or row['Region'] == 'Sri Lanka':
            year_data = row['Year']
            POp_data = row['Population']

            if year_data not in Year_list:
                Year_list[row['Year']] = float(row['Population'])
            else:
                Year_list[row['Year']] += float(row['Population'])
    return Year_list


def Transfer(Pop_Data_Add):
    Pop_Year = list(Pop_Data_Add.keys())
    Total_pop = list(Pop_Data_Add.values())
    return Pop_Year, Total_pop


def Graph(Pop_year, Total_pop):
    plt.bar(Pop_year, Total_pop)
    plt.xticks(rotation = 90)
    plt.tight_layout()
    plt.show()


def main():
    file_handle = open('population-estimates_csv.csv', mode='r')
    Pop_Data = DictReader(file_handle)
    Pop_Data_Add = Calculate(Pop_Data)
    Pop_year, Pop_pop = Transfer(Pop_Data_Add)
    Graph(Pop_year, Pop_pop)


if __name__ == "__main__":
    main()
