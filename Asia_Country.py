# import csv

# import matplotlib.pyplot as plt

# Asean_con = {}
# with open('population-estimates_csv.csv',mode='r') as file:
#     data = csv.DictReader(file)

#     for row in data:
#         if row['Region'] == "Brunei Darussalam" or row['Region'] == "Cambodia" or row['Region'] == "Indonesia" or row['Region'] == "Myanmar" or row['Region'] == "Philippines" or row['Region'] == "Singapore" or row['Region'] == "Thailand" or row['Region'] == "Viet Nam":
#             if int(row['Year']) == 2014:
#                 if Asean_con.get(row['Region'])== None:
#                     Asean_con[row['Region']] = row['Population']
#                 else:
#                     Asean_con[row['Region']] += row['Population']

# plt.bar(list(Asean_con.keys()),list(Asean_con.values()))
# plt.xticks(rotation = 90)
# plt.tight_layout()
# plt.show()

import csv
import matplotlib.pyplot as plt
from csv import DictReader


def Calculate(Pop_Data):
    Asean_Con = {}
    for row in Pop_Data:
        if row['Region'] == "Brunei Darussalam" or row['Region'] == "Cambodia" or row['Region'] == "Indonesia" or row['Region'] == "Myanmar" or row['Region'] == "Philippines" or row['Region'] == "Singapore" or row['Region'] == "Thailand" or row['Region'] == "Viet Nam":
            if int(row['Year']) == 2014:
                if row['Region'] not in Asean_Con:
                    Asean_Con[row['Region']] = row['Population']
                else:
                    Asean_Con[row['Region']] += row['Population']
    return Asean_Con


def Transfer(Asia_Country):
    Country_Name = list(Asia_Country.keys())
    Country_Pop = list(Asia_Country.values())
    return Country_Name, Country_Pop


def Graph(Country_name, Country_pop):
    plt.bar(Country_name, Country_pop)
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.show()


def main():
    Csv_file = open('population-estimates_csv.csv', mode='r')
    Pop_Data = DictReader(Csv_file)

    Asia_country = Calculate(Pop_Data)
    Country_Name, Country_pop = Transfer(Asia_country)
    Graph(Country_Name, Country_pop)


if __name__ == "__main__":
    main()
